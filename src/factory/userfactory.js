'use strict'

const nodemailer = require('nodemailer');
const redisClient = require('../../server/redis_client');
const user = require('../model/user');
const User = require("../model/user");
const {hashPassword} = require('../utils/userAuth')

var otp = ''

var transport = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: "aishadeshmukh2021@gmail.com",
      pass: "aisha2021"
    }
    //port: 2525,
    
});

exports.signUp = (request, h) => {

  console.log("User creation!")
  const password = hashPassword(request.payload.password);
  const data = {
    name: request.payload.name,
    email: request.payload.email,
    password: password
  }
  
  const user = new User({
    name: request.payload.name,
    email: request.payload.email,
    password: password
  });

  user.save((function (err) {
    if (err){
      console.log(err)
    return;
    }
    return user;
  }))
  return user;  

}

exports.login = (request, h) => {

  let password = request.payload.password;
  const email = request.payload.email;

  const comparePassword = (password) => {
    let flag = 0;
    console.log("PASSWORD CHECKING")
    password = hashPassword(password)
    if(User.findOne({attributes: [
        'email',
        'password'
    ], where: {email, password}})) {
        flag = 1
     //   console.log('FLAG' + flag)
        return flag;
    }
    else
    return flag;
  };

  User.findOne({ where: { email } }).then((user) => {
    if (!user) return (null, false, { message: `There is no record of the email ${email}.` });
  })


  if (!comparePassword(password)){
    return(null, false, { message: 'Your email/password combination is incorrect.' });
  } else {
      
    // OTP GENERATION PART
    otp = Math.random();
    otp = otp * 1000000;
    otp = parseInt(otp);
    console.log(otp);


    let message = {
        from: 'fb1bbc7206-9c5c51@inbox.mailtrap.io', // Sender address
        to: email,
        subject: 'OTP for login',
        text: `OTP is ${otp}`
    }

    redisClient.set(email, otp)
    redisClient.get(email, function (err, result) {
        if (err) {
          console.error(err);
        } else {
          console.log(result); // Promise resolves to "bar"
        }
      });   
      
      transport.sendMail(message, function(err, info) {
        if (err) {
          console.log(err)
          return 'Could not send email!'
        } else {
        //  console.log(info);

         User.updateOne({
            email: email,
            password: hashPassword(password)
          },{
             $set: {
              'otp': otp
             } 
          }, function(err){
            console.log(err)
          })
          return 'Email sent successfully !!!'
        }

      });
    }
    
}

exports.verify = async(request, h) => {

    var email = request.payload.email;
    var userOTP = request.payload.otp;
    
    
    User.findOne({ email: email, otp: userOTP }).then((user) => {
      if (!user) {
        console.log('INVALID OTP')
        return (null, false, { message: `OTP ${userOTP} INVALID. TRY AGAIN !!!` });
      }
      else{
        console.log('valid otp')
        return ' VALID OTP. LOGIN SUCCESSFULL !!!'

      }      
    }).then(() => {
      return 'VALID'
    })
  
}