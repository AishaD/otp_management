'use strict'

const {signUp, login, verify} = require('../factory/userfactory')
const {responseFunction } = require('../utils/response')
const config = require('../../config/msg.json')

exports.signUp = {

    description: 'SignUp API',
    notes: 'SignUp API',
    tags: ['api', 'SignUp API'] ,
    handler: (request, h) => {

        return responseFunction(config.signup, config.status, signUp(request))
    }
}

exports.login = {

    description: 'Login API',
    notes: 'Login API',
    tags: ['api', 'Login API'] ,
    handler: (request, h) => {
        
        return responseFunction(config.login, config.status, login(request))

    }
}

exports.verify = {

    description: 'OTP Verification API',
    notes: 'OTP Verification API',
    tags: ['api', 'OTP Verification API'] ,
    handler: async(request, h) => {

        return responseFunction(config.verify, config.status, await verify(request))
    }
}