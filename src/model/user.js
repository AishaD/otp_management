'use strict'
var mongoose  = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true
    },
    email: {
        type: String,
        trim: true,
        required:  true
    },
    password: {
      type: String,
      trim: true,
    },
    otp: {
      type: Number
    }
}, { timestamps: true});


module.exports = mongoose.model("User", userSchema);