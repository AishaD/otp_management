'use strict'

const userFunctions = require('../controller/usercontroller')


module.exports = [
    {method: 'POST', path: '/signup', config: userFunctions.signUp},
    {method: 'POST', path: '/login', config: userFunctions.login},
    {method: 'POST', path: '/login/verify', config: userFunctions.verify}
]
