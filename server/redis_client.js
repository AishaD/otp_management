'use strict'

const config = require('../config/config.json')
let redis     = require('redis')
let redisClient    = redis.createClient({
    port      : 6379,               
    host      : 'localhost'      
   // password  : config.password,    
});

console.log('redis client')
redisClient.on('ready', function(){
  console.log("Connected to Redis")
})
redisClient.on('error', function (err) {
  console.log(err)
})
module.exports = redisClient;